from django.contrib import admin
from .models import Food, Drink, Order

# Register your models here.
class OrderAdmin(admin.ModelAdmin):
    list_display = ['order_id', 'food', 'drink', 'created_at', 'updated_at']
    list_filter = ['created_at', 'updated_at']
    search_fields = ['order_id', 'food', 'drink']
class FoodAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'created_at', 'updated_at', 'is_active']
    list_filter = ['is_active', 'created_at', 'updated_at']
    search_fields = ['name', 'price']

class DrinkAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'created_at', 'updated_at', 'is_active']
    list_filter = ['is_active', 'created_at', 'updated_at']
    search_fields = ['name', 'price']


admin.site.register(Order, OrderAdmin)
admin.site.register(Food, FoodAdmin)
admin.site.register(Drink, DrinkAdmin)
