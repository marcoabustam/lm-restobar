from django.shortcuts import render

def indexOrder(request):
    return render(request, 'index.html')